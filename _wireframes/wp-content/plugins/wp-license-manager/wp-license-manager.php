<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           wp_license_manager
 *
 * @wordpress-plugin
 * Plugin Name:       WP License Manager
 * Plugin URI:        http://www.blumilk.com
 * Description:       WP License Manager
 * Version:           1.0.0
 * Author:            Blumilk
 * Author URI:        http://www.blumilk.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-license-manager
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wp-license-manager-activator.php
 */
function activate_wp_license_manager() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-license-manager-activator.php';
	wp_license_manager_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wp-license-manager-deactivator.php
 */
function deactivate_wp_license_manager() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-license-manager-deactivator.php';
	wp_license_manager_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wp_license_manager' );
register_deactivation_hook( __FILE__, 'deactivate_wp_license_manager' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wp-license-manager.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wp_license_manager() {

	$plugin = new wp_license_manager();
	$plugin->run();

}
run_wp_license_manager();
