// @codekit-prepend "html5shiv.min.js";

jQuery(document).ready(function($) {
	// Remove empty p tags
	$("p:empty").remove();
	
	// Responsive navigation
	function responsiveMenu() {
		var windowWidth = $(window).innerWidth();
		
		if(windowWidth < 1441) {
			$("#menu-mobile").html($("#menu-desktop").html());
		} else {
			$("#menu-mobile").empty();
		}
	}
	responsiveMenu();
	
	$("#menu-trigger").click(function() {
		if ($("#menu-mobile").hasClass("expanded")) {
			$("#menu-mobile.expanded").removeClass("expanded").slideUp(1000);
			$("#menu-trigger span").removeClass("open");
			$(".menu-button").removeClass("open");
			$("html").removeClass("body-overflow");
		} else {
			$("#menu-mobile").addClass("expanded").slideDown(1000);
			$("#menu-trigger span").addClass("open");
			$(".menu-button").addClass("open");
			$("html").addClass("body-overflow");
		}
	});
	
	$(window).resize(function() {
		responsiveMenu();
	});
});