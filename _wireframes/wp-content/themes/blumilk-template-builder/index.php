<?php get_header(); ?>
<?php if(have_posts()) { ?>
	<div class="wrapper">
		<div class="row">
			<div class="dt-12">
				<h1>Modest Grid WordPress</h1>
				<hr class="size-xl">
				<?php while(have_posts()) {
					the_post(); ?>
					<article>
						<h1>H1 Title</h1>
						<hr class="size-xl">
						<h2>H2 Title</h2>
						<hr class="size-l">
						<h3>H3 Title</h3>
						<hr class="size-m">
						<h4>H4 Title</h4>
						<hr class="size-s">
						<h5>H5 Title</h5>
						<hr>
						<h6>H6 Title</h6>
						<form>
							<input class="border-p" type="text" placeholder="Name">
							<input class="primary border-p size-l" type="submit">
						</form>
						<?php the_content(); ?>
					</article>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } else { ?>
	<div class="wrapper">
		<div class="row">
			<div class="dt-12">
				<h1>No posts to display</h1>
			</div>
		</div>
	</div>
<?php } ?>
<?php get_footer(); ?>