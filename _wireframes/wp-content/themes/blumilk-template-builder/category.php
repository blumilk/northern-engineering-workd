<?php get_header(); ?>
<?php if(have_posts()) { ?>
	<div class="wrapper">
		<div class="row">
			<div class="dt-12">
				<h2>Category Archive: <?php echo single_cat_title('', false); ?></h2>
				<?php while(have_posts()) {
					the_post(); ?>
					<article>
						<h2>
							<a href="<?php the_permalink(); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
								<?php the_title(); ?>
							</a>
						</h2>
						<?php the_content(); ?>
					</article>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } else { ?>
	<div class="wrapper">
		<div class="row">
			<div class="dt-12">
				<h1>No posts to display in <?php echo single_cat_title( '', false ); ?></h1>
			</div>
		</div>
	</div>
<?php } ?>
<?php get_footer(); ?>