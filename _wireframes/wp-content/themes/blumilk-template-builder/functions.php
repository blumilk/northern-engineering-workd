<?php
	// Remove the admin bar for logged in users.
	add_filter('show_admin_bar', '__return_false');
	
	// Adding CSS styles
	add_action('wp_enqueue_scripts', 'modest_styles');
	function modest_styles() {
		wp_register_style('screen', get_stylesheet_directory_uri() . '/css/style.css', '', '', 'all');
		wp_enqueue_style('screen');
	}
	
	// Adding JavaScript scripts
	add_action('wp_enqueue_scripts', 'modest_scripts');
	function modest_scripts() {
		wp_enqueue_script('custom-script', get_stylesheet_directory_uri() . '/js/min/site-min.js', array('jquery'));
	}
	
	// Register the navigation menu.
	add_action('init', 'modest_menu');
	function modest_menu() {
		register_nav_menu('modest-menu',__('Modest Menu'));
	}
	
	// Show Featured Image on posts.
	add_theme_support('post-thumbnails');
	
	// Advanced Custom Fields Option Page
	if(function_exists('acf_add_options_page')) {
		acf_add_options_page(array(
			'page_title' => 'Contact Details',
			'menu_title' => 'Contact Details',
			'menu-slug' => 'contact-details',
			'icon_url' => 'dashicons-phone'
		));
	}
	
	// Excerpt Length
	add_filter('excerpt_length', 'custom_excerpt_length', 999);
	function custom_excerpt_length($length) {
		return 18;
	}
	
	// Adjust the length of the WordPress content you would like to show
	// the_content_length(360, "...");
	function the_content_length($length, $trail = false) {
		$theContent = apply_filters("the_content", get_the_content());
		$theContent = str_replace("]]>", "]]&gt;", $theContent);
		
		if(strlen($theContent) < $length) {
			echo $theContent;
		} else {
			// Checks to see if there's a value for $trail
			if(!empty($trail)) {
				echo trim(substr($theContent, 0, $length)) . $trail;
			} else {
				echo trim(substr($theContent, 0, $length)) . "...";
			}
		}
	}
	
	// Removes squared brackets from the reset password link
	// Very useful when you require emails to be formatted to HTML
	function removeBracketsHTML($message, $key, $user_login, $user_data) {
		return preg_replace('#<(https?:\/\/.+?)>#i', '$1', $message);
	}
	add_filter('retrieve_password_message', 'removeBracketsHTML', 100, 4);
	
	// Loads the login.css stylesheet for when a client is logging in
	function loginStylesJGD() {
		echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/css/login.css" />';
	}
	add_action('login_head', 'loginStylesJGD');
	
	function logoURLJGD() {
		return 'http://www.blumilk.com';
	}
	add_filter('login_headerurl', 'logoURLJGD');
	
	// Loads the login.css stylesheet for when a client is logging in
	function adminStylesJGD() {
		echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/css/login.css" />';
	}
	add_action('admin_head', 'adminStylesJGD');
	
	
		// Custom post types
	function create_post_type() {
		
		// stations
		register_post_type ('stations',
			array(
				'labels' => array(
					'name' => __('Stations'),
					'singular_name' => __('Station'),
					'add_new_item' => __('Add New Station'),
					'add_new' => _x('Add New', 'Station'),
					'edit_item' => __('Edit Station'),
					'new_item' => __('New Station'),
					'all_items' => __('All Stations'),
					'view_item' => __('View Station'),
					'search_items' => __('Search Stations'),
					'not_found' =>  __('No Stations Found'),
					'not_found_in_trash' => __('No Stations Found in the Bin'),
					'parent_item_colon' => '',
					'menu_name' => 'Stations',
					'add_new' => 'Add Station'
				),
				'public' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-id-alt',
				'menu_position' => 4,
				'hierarchical' => true,
				'supports' => array(
					'title',
					'editor',
					'revisions',
					'author',
					'custom-fields'
				),
				'rewrite' => array(
					'slug' => 'stations',
				),
			)
		);
		
		
		// route
		register_post_type ('routes',
			array(
				'labels' => array(
					'name' => __('Routes'),
					'singular_name' => __('Route'),
					'add_new_item' => __('Add New Route'),
					'add_new' => _x('Add New', 'Route'),
					'edit_item' => __('Edit Route'),
					'new_item' => __('New Route'),
					'all_items' => __('All Routes'),
					'view_item' => __('View Route'),
					'search_items' => __('Search Routes'),
					'not_found' =>  __('No Routes Found'),
					'not_found_in_trash' => __('No Routes Found in the Bin'),
					'parent_item_colon' => '',
					'menu_name' => 'Routes',
					'add_new' => 'Add Route'
				),
				'public' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-id-alt',
				'menu_position' => 4,
				'hierarchical' => true,
				'supports' => array(
					'title',
					'editor',
					'revisions',
					'author',
					'custom-fields'
				),
				'rewrite' => array(
					'slug' => 'routes',
				),
			)
		);
		
		// route
		register_post_type ('engineering_work',
			array(
				'labels' => array(
					'name' => __('Engineering Work'),
					'singular_name' => __('Engineering Work'),
					'add_new_item' => __('Add Engineering Work'),
					'add_new' => _x('Add New', 'Engineering Work'),
					'edit_item' => __('Edit Engineering Work'),
					'new_item' => __('New Engineering Work'),
					'all_items' => __('All Engineering Work'),
					'view_item' => __('View Engineering Work'),
					'search_items' => __('Search Routes'),
					'not_found' =>  __('No Routes Found'),
					'not_found_in_trash' => __('No Routes Found in the Bin'),
					'parent_item_colon' => '',
					'menu_name' => 'Engineering Work',
					'add_new' => 'Add Engineering Work'
				),
				'public' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-id-alt',
				'menu_position' => 4,
				'hierarchical' => true,
				'supports' => array(
					'title',
					'editor',
					'revisions',
					'author',
					'custom-fields'
				),
				'rewrite' => array(
					'slug' => 'routes',
				),
			)
		);
		
	}
	add_action('init', 'create_post_type');	

?>