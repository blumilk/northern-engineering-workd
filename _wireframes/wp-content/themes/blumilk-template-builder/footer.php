		<footer>
			<section class="wrapper">
				<div class="row nested">
					<div class="tl-6">
						<h6>Contact Us</h6>
						<form action="" method="post" class="row">
							<div class="tp-6">
								<input class="<?php the_sub_field('background_colour'); ?>" type="text" placeholder="Name">
							</div>
							<div class="tp-6">
								<input class="<?php the_sub_field('background_colour'); ?>" type="text" placeholder="Email">
							</div>
							<div class="tp-12">
								<textarea placeholder="Message"></textarea>
							</div>
							<div class="tp-6 force">
								<input class="primary" type="submit" value="Send">
							</div>
						</form>
					</div>
					<div class="tl-3">
						<h6>Quick Links</h6>
						<ul>
							<li>
								Link 1
							</li>
							<li>
								Link 2
							</li>
							<li>
								Link 3
							</li>
							<li>
								Link 4
							</li>
						</ul>
					</div>
					<div class="tl-3">
						<h6>Connect With Us</h6>
						<ul>
							<li>
								Link 1
							</li>
							<li>
								Link 2
							</li>
							<li>
								Link 3
							</li>
							<li>
								Link 4
							</li>
						</ul>
					</div>
				</div>
			</section>
		</footer>
		<?php wp_footer(); ?>
	</body>
</html>