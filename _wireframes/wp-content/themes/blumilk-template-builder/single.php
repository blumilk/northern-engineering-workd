<?php get_header(); ?>
<?php if(have_posts()) { ?>
	<div class="wrapper">
		<div class="row">
			<div class="dt-12">
				<?php while(have_posts()) {
					the_post(); ?>
					<article>
						<div class="row">
							<div class="dt-4">
								<h2><?php the_title(); ?></h2>
							</div>
							<div class="dt-8">
								<?php the_content(); ?>
							</div>
						</div>
					</article>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } ?>
<?php get_footer(); ?>