<?php get_header(); ?>
<?php if(have_posts()) { ?>
	<div class="wrapper">
		<div class="row">
			<div class="dt-12">
				<h2>Search Results for '<?php echo get_search_query(); ?>'</h2>
				<?php while(have_posts()) {
					the_post(); ?>
					<article>
						<h2>
							<a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
								<?php the_title(); ?>
							</a>
						</h2>
						<?php the_content(); ?>
					</article>
				<?php } ?>
			</div>
		</div>
	</div>
<?php } else { ?>
	<div class="wrapper">
		<div class="row">
			<div class="dt-12">
				<h1>No results found for '<?php echo get_search_query(); ?>'</h1>
			</div>
		</div>
	</div>
<?php } ?>
<?php get_footer(); ?>