<?php
	/*
		Template Name: Get
	*/
	
	// Array with names
	$args = array(
		'post_type' 	=> 'post',
		'orderby' 		=> 'date',
		'order' 		=> 'ASC',
	);
	
	$a = array();
	
	$adminSearch = new WP_Query($args);
	if($adminSearch->have_posts()) {
		while($adminSearch->have_posts()) {
			$adminSearch->the_post();
			array_push($a, array(
				'title' 		=> get_the_title(),
				'permalink' 	=> get_the_permalink(),
				'image'			=> get_the_post_thumbnail(),
				'custom' 		=> get_field('additional_search'),
			));
		}
	}
	
	// get the q parameter from URL
	$keywords = $_REQUEST['keywords'];
	
	$hint = '';
	
	// lookup all hints from array if $keywords is different from ''
	if($keywords !== '') {
		$keywords = strtolower($keywords);
		$len=strlen($keywords);
		foreach($a as $name) {
			if(stristr($keywords, substr($name['title'], 0, $len)) || stristr($keywords, substr($name['custom'], 0, $len))) {
				if($name['image']) {
					$hint .= '<a href="' . $name['permalink'] . '"><li class="row"><div class="dt-1">' . $name['image'] . '</div><div class="dt-11">' . $name['title'] . '</div></li></a>';
				} else {
					$hint .= '<a href="' . $name['permalink'] . '"><li>' . $name['title'] . '</li></a>';
				}
			}
		}
	}
	
	// Output "no suggestion" if no hint was found or output correct values
	if($hint === '') {
		echo '<li>No results found.</li>';
	} else {
		echo $hint;
	}
?>