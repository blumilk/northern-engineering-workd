<section class="comp comp-00100 <?php the_sub_field('background_colour'); ?>">
	<div class="wrapper <?php the_sub_field('width'); ?> no-gaps">
		<div class="row nested">
			<div class="dt-12">
				<?php the_sub_field('body_copy'); ?>
			</div>
		</div>
	</div>
</section>