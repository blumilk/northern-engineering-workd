<section class="comp comp-30202 <?php the_sub_field('background_colour'); ?> <?php the_sub_field('text_colour'); ?>">
	<div class="wrapper">
		<div class="row">
			<div class="dt-12 align-central">
				<h3>Get in touch</h3>
			</div>
			<div class="tl-6">
				<?php
					$url = "http://maps.googleapis.com/maps/api/geocode/xml?address=" . get_sub_field('postcode') . "+UK&sensor=false";
						
					$result = simplexml_load_file($url);
					
					foreach($result->result as $mydata) {
						foreach($mydata->geometry as $values) {
							foreach($values->location as $bodies) {
								$lat = (string) $bodies->lat;
								$long = (string) $bodies->lng;
							}
						}
					}
				?>
				<script src="http://maps.google.com/maps/api/js?key=AIzaSyBHJVtpwIKlVJP58zOtDus7ISPUMWJYrZ8" type="text/javascript"></script>
				<script type="text/javascript">
					jQuery(document).ready(function() {
						initialize();
					});
					
					function initialize() {
						var myLatLng = {lat: <?php echo $lat; ?>, lng: <?php echo $long; ?>};
					
						var map_options = {
							center: myLatLng,
							zoom: 15,
							mapTypeId: google.maps.MapTypeId.MAP,
							styles: [
							    {
							        "featureType": "water",
							        "elementType": "geometry",
							        "stylers": [
							            {
							                "color": "#e9e9e9"
							            },
							            {
							                "lightness": 17
							            }
							        ]
							    },
							    {
							        "featureType": "landscape",
							        "elementType": "geometry",
							        "stylers": [
							            {
							                "color": "#f5f5f5"
							            },
							            {
							                "lightness": 20
							            }
							        ]
							    },
							    {
							        "featureType": "road.highway",
							        "elementType": "geometry.fill",
							        "stylers": [
							            {
							                "color": "#ffffff"
							            },
							            {
							                "lightness": 17
							            }
							        ]
							    },
							    {
							        "featureType": "road.highway",
							        "elementType": "geometry.stroke",
							        "stylers": [
							            {
							                "color": "#ffffff"
							            },
							            {
							                "lightness": 29
							            },
							            {
							                "weight": 0.2
							            }
							        ]
							    },
							    {
							        "featureType": "road.arterial",
							        "elementType": "geometry",
							        "stylers": [
							            {
							                "color": "#ffffff"
							            },
							            {
							                "lightness": 18
							            }
							        ]
							    },
							    {
							        "featureType": "road.local",
							        "elementType": "geometry",
							        "stylers": [
							            {
							                "color": "#ffffff"
							            },
							            {
							                "lightness": 16
							            }
							        ]
							    },
							    {
							        "featureType": "poi",
							        "elementType": "geometry",
							        "stylers": [
							            {
							                "color": "#f5f5f5"
							            },
							            {
							                "lightness": 21
							            }
							        ]
							    },
							    {
							        "featureType": "poi.park",
							        "elementType": "geometry",
							        "stylers": [
							            {
							                "color": "#dedede"
							            },
							            {
							                "lightness": 21
							            }
							        ]
							    },
							    {
							        "elementType": "labels.text.stroke",
							        "stylers": [
							            {
							                "visibility": "on"
							            },
							            {
							                "color": "#ffffff"
							            },
							            {
							                "lightness": 16
							            }
							        ]
							    },
							    {
							        "elementType": "labels.text.fill",
							        "stylers": [
							            {
							                "saturation": 36
							            },
							            {
							                "color": "#333333"
							            },
							            {
							                "lightness": 40
							            }
							        ]
							    },
							    {
							        "elementType": "labels.icon",
							        "stylers": [
							            {
							                "visibility": "off"
							            }
							        ]
							    },
							    {
							        "featureType": "transit",
							        "elementType": "geometry",
							        "stylers": [
							            {
							                "color": "#f2f2f2"
							            },
							            {
							                "lightness": 19
							            }
							        ]
							    },
							    {
							        "featureType": "administrative",
							        "elementType": "geometry.fill",
							        "stylers": [
							            {
							                "color": "#fefefe"
							            },
							            {
							                "lightness": 20
							            }
							        ]
							    },
							    {
							        "featureType": "administrative",
							        "elementType": "geometry.stroke",
							        "stylers": [
							            {
							                "color": "#fefefe"
							            },
							            {
							                "lightness": 17
							            },
							            {
							                "weight": 1.2
							            }
							        ]
							    }
							]
						};
					
						var google_map = new google.maps.Map(document.getElementById("map"), map_options);
					
						var info_window = new google.maps.InfoWindow({
							content: 'loading'
						});
					
						var marker = new google.maps.Marker({
							position: myLatLng,
							map: google_map,
							icon: '<?php the_sub_field('pin_icon'); ?>',
						});
					}
				</script>
				<div id="map" style="width: 100%; height: 300px;">
					Google Map
				</div>
			</div>
			<div class="tl-6">
				<?php if(have_rows('address', 'options')) { ?>
					<div class="address">
						<?php while(have_rows('address', 'options')) {
							the_row(); ?>
							<p><?php the_sub_field('line'); ?></p>
						<?php } ?>
					</div>
				<?php } ?>
				<?php if(get_field('telephone_number', 'options')) { ?>
					<div class="contact">
						<strong>Telephone</strong>
						<p>
							<a href="tel:<?php the_field('telephone_number', 'options'); ?>">
								<?php the_field('telephone_number', 'options'); ?>
							</a>
						</p>
					</div>
				<?php } ?>
				<?php if(get_field('fax_number', 'options')) { ?>
					<div class="contact">
						<strong>Fax</strong>
						<p><?php the_field('fax_number', 'options'); ?></p>
					</div>
				<?php } ?>
				<?php if(get_field('email_address', 'options')) { ?>
					<div class="contact">
						<strong>Email</strong>
						<p>
							<a href="mailto:<?php the_field('email_address', 'options'); ?>">
								<?php the_field('email_address', 'options'); ?>
							</a>
						</p>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</section>