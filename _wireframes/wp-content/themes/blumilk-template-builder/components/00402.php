<section class="comp comp-00402 <?php the_sub_field('background_colour'); ?> <?php the_sub_field('text_colour'); ?>">
	<div class="wrapper">
		<div class="row">
			<div class="tl-4">
				<?php $image = get_sub_field('image'); ?>
				<img src="<?php echo $image['url']; ?>">
			</div>
			<div class="tl-7 indent-tl-1">
				<strong><?php the_sub_field('bold_text'); ?></strong>
				<?php the_sub_field('content'); ?>
			</div>
		</div>
	</div>
</section>