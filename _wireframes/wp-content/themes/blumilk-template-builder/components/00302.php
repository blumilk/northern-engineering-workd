<section class="comp comp-00302 <?php the_sub_field('background_colour'); ?> <?php the_sub_field('text_colour'); ?>">
	<div class="wrapper">
		<div class="row">
			<div class="tl-6">
				<?php the_sub_field('column_one'); ?>
			</div>
			<div class="tl-6">
				<?php the_sub_field('column_two'); ?>
			</div>
		</div>
	</div>
</section>