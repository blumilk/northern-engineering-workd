<section class="comp comp-70201">
	<div class="wrapper">
		<div class="row">
			<div class="dt-12 align-central">
				<h3>Journey Planner</h3>
			</div>
			
			<!--  popup -->
			<div id="modal" class="modal">
				<div id="modal-content" class="modal-content">
				</div>
			</div>
			<?php
				
				$station_list = array();
				
				$query = new WP_Query ( array( 'post_type' => 'stations', 'posts_per_page'   => -1, 'orderby'=> 'title', 'order' => 'ASC' ) );
				if ($query->have_posts()) {
					while ( $query->have_posts() ) {
						$query->the_post();
						
						$name = get_the_title();

						$station_list[$name] = array(
							'name' => $name,
							'content' => get_the_content()
							);
					}
				}

				$route_list = array();
				$route_count = 0;
				$query = new WP_Query ( array( 'post_type' => 'routes', 'posts_per_page'   => -1, 'orderby'=> 'title', 'order' => 'ASC' ) );
				if ($query->have_posts()) {
					while ( $query->have_posts() ) {
						$query->the_post();
						$name = htmlspecialchars(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8'));;
						$route_list[$name] = array();
						
						$stations_on_route = get_field('stations');

						$pos_y = 0;
						$pos_x = 0;
						
						foreach ($stations_on_route as $key => $value) {
							if ($value['position_x'] != 0 && $value['position_y'] != 0) {
								$pos_x = $value['position_x'];
								$pos_y = $value['position_y'];
							} else {
								$pos_x = 300;
								$pos_y += 100;
							}
							
							$route_list[$name][] = array(
									'name' => $value['station']->post_title,
									'pos_x' => $pos_x,
									'pos_y' => $pos_y,
									'text_position' => $value['text_position'],
								);
						}
					}
				}
				
				
				$work_list = array();
				
				$query = new WP_Query ( array( 'post_type' => 'engineering_work', 'posts_per_page'   => -1, 'orderby'=> 'title', 'order' => 'ASC' ) );
				if ($query->have_posts()) {
					while ( $query->have_posts() ) {
						$query->the_post();

						$work_list[] = array(
							'name' => get_the_title(),
							'from' => get_field('from')->post_title,
							'to' => get_field('to')->post_title,
							'content' => get_the_content()
							);
					}
				}
			?>	
			<div id="routemap2" class="tl-12">
				<div class="station green tl-12">
					<div class="outer-circle">
						<div class="inner-circle">
							<div class="status-circle green">
								
							</div>
			  			</div>
			  			<div class="station-details">
							&nbsp;
						</div>
					</div>
				</div>
				<div class="route green tl-12">
					<div class="left-line">
						<div class="right-line">
							
						</div>
					</div>
				</div>
				<div class="station yellow tl-12">
					<div class="outer-circle">
						<div class="inner-circle">
							<div class="status-circle yellow">
								
							</div>
			  			</div>
					</div>
				</div>
				<div class="route yellow tl-12">
					<div class="left-line">
						<div class="right-line">
							
						</div>
					</div>
				</div>
				<div class="station red tl-12">
					<div class="outer-circle">
						<div class="inner-circle">
							<div class="status-circle red">
								
							</div>
			  			</div>
					</div>
				</div>
				<div class="route red tl-12">
					<div class="left-line">
						<div class="right-line">
							
						</div>
					</div>
				</div>
				
				
				<?php
					/*
					$test = 0;
					foreach($route_list as $route_list_key => $route_list_value) {
						if ($test == 0)
							echo var_dump(array($route_list_key));
						else
							$test++;
						
					}
					*/
				?>
			</div>
			<br / ><br / ><br / ><br / >

			<div id="routemap" class="tl-12">
				<script src="<?php echo get_template_directory_uri(); ?>/js/createjs.min.js"></script>
				<script type="text/javascript">
					jQuery(document).ready(function() {
						init();
					});
					
					class Point {
						constructor(x, y) {
							this.setPos(x, y);
						}
						
						setPos(x, y) {
							this._x = x;
							this._y = y;
						}
					}
					
					class Rectangle {
						constructor(height, width) {
							this._height = height;
							this._width = width;
						}
						
						get left() {
							return 0;
						}
						
						get right() {
							return this._width;
						}
						
						get top() {
							return 0;
						}
						
						get bottom() {
							return this._height;
						}
					}
					
					class Station {
						constructor(name) {
							this._name = name;
						}
					}
					
					class Route {
						constructor(name) {
							this._name = name;
							this._route_stations = new Array();
						}
					}

					class Works {
						constructor(name) {
							this._name = name;
							this._work_stations = new Array();
						}
					}
					
					class Mouse {
						constructor() {
							this._pos = new Point(0, 0);
							this._last_pos = new Point(0, 0);
							
							this._mouse_down = false;
							this._mouse_down_at = new Point(0,0);
							this._mouse_up_at = new Point(0,0);
						}
						
						mouseDown(x, y) {
							this._mouse_down = true;
							this._mouse_down_at.setPos(x, y);
						}
						
						mouseUp(x, y) {
							this._mouse_down = false;
							this._mouse_up_at.setPos(x, y);
						}
						
						mouseMove(x, y) {
							this._last_pos.setPos(this._pos._x, this._pos._y);
							this._pos.setPos(x, y);
						}
					}
					
					
					class Scroll {
						constructor() {
							this._focus_point = new Point(0,0);
							this._boundry = new Rectangle (100, 100);
						}
					}

					class RouteMap {
						constructor() {
							this._start_time = new Date().getTime();
							this._end_time = new Date().getTime();
							this._mouse = new Mouse();
							this._scroll = new Scroll();
							this._canvas = document.getElementById("myCanvas");;
							this._stage = '';
							this._zoom_level = 10;
							
							this._all_route_data = new Array();
							this._all_station_data = new Array();
							this._all_work_data = new Array();
							
							this.stations = new Array();
							this.routes = new Array();
						}
					}

					var routemap = new RouteMap();
					
					var config = new Array();
					
					function init() {
						routemap._stage = new createjs.Stage("myCanvas");
						createjs.Touch.enable(routemap._stage);
						
						routemap._stage.on("stagemousedown", function(e) {
							routemap._mouse.mouseDown(e.stageX, e.stageY);
						});
						
						routemap._stage.on("stagemouseup", function(e) {
							routemap._mouse.mouseUp(e.stageX, e.stageY);
						});
						
						routemap._stage.on("stagemousemove", function(e) {
							routemap._mouse.mouseMove(e.stageX, e.stageY);
							
							if (routemap._mouse._mouse_down) {
								var x_diff = routemap._mouse._pos._x - routemap._mouse._last_pos._x;
								var y_diff = routemap._mouse._pos._y - routemap._mouse._last_pos._y;
								
								if (x_diff != 0 && y_diff != 0) {
									moveWorld(x_diff, y_diff)
								}
							}
						});
						
						resizeCanvas();
						window.addEventListener("resize", resizeCanvas, false);
						
						createjs.Ticker.addEventListener("tick", handleTick);
						
						loadData();
					}
					
					function resizeCanvas(e) {
					  	routemap._canvas = document.getElementById("myCanvas");
						routemap._canvas.width = document.getElementById("routemap").offsetWidth - 2;
						routemap._canvas.height = document.getElementById("routemap").offsetWidth / 2;
					}
					
					function loadData() {
						routemap._all_station_data = <?php echo json_encode( $station_list ); ?>;
						routemap._all_route_data = <?php echo json_encode( $route_list ); ?>;
						routemap._all_work_data = <?php echo json_encode( $work_list ); ?>;
					}
					
					function clearStage() {
						for (var i = 0; i < routemap.stations.length; i++) {
							routemap._stage.removeChild(routemap.stations[i].station_icon);
							routemap._stage.removeChild(routemap.stations[i].station_text);
						}
						
						for (var i = 0; i < routemap.routes.length; i++) {
							
							routemap._stage.removeChild(routemap.routes[i]);
						}

					}
					
					function showRoute(route_number) {
						clearStage();
						var selected_route = routemap._all_route_data[route_number];
						routemap.stations = new Array();
						routemap.routes = new Array();

						selected_route.forEach(setupStation);
						
						var last_station = '';
						
						if (routemap.stations.length > 1) {
							for (var i = 0; i < routemap.stations.length; i++) {
								var station_data = routemap.stations[i];
								
								if (i == 0) {
									last_station = station_data;
								} else {
									var connection = new createjs.Shape();
									
									var graphics = connection.graphics;
									
									graphics.setStrokeStyle(10);
									
									var checked_connection = checkConnection(last_station['name'], station_data['name']);
									graphics.beginStroke(checked_connection['color']);
									
									graphics.moveTo(last_station['pos_x'], last_station['pos_y']);
									graphics.lineTo(station_data['pos_x'], station_data['pos_y']);
									graphics.endStroke();
									graphics.alpha = 0.5;
									
									connection.station_from = last_station['name'];
									connection.station_to = station_data['name'];
									
									if (checked_connection['content'].name != undefined) {
										connection.name = checked_connection['content'].name;
										connection.content = checked_connection['content'].content;
									}

									routemap._stage.addChild(connection);
								
									connection.on("click", function() { showRouteDetails(this) });
									routemap.routes.push(connection);
									
									last_station = station_data;

								}
							}
						}
						
						routemap.stations.forEach(showStation);
					}
					
					function checkConnection(from_name, to_name) {
						var status = new Array();
						status['color'] = "rgba(0,255,0,1)";
						status['content'] = '';
						
						for (var i = 0; i < routemap._all_work_data.length; i++) {
							if (from_name == routemap._all_work_data[i]['from'] && to_name == routemap._all_work_data[i]['to']) {
								status['color'] = "rgba(255,0,0,1)";
								status['content'] = routemap._all_work_data[i];
							}
						}
						
						return status;
					}


					function showRouteDetails(route) {
						var modal = document.getElementById('modal');
						var modal_content = document.getElementById('modal-content');
						
						var html = "";
						html += '<button id="modal-button" class="modal-button">X</button><br />';
						
						html += route.name + '<br /><br />';
						
						if (route.content)
							html += route.content;
						
						
						modal.style.display = "block";
						modal_content.innerHTML = html;
						
						var modal_button = document.getElementById('modal-button');

						modal_button.onclick = function() {
						    modal.style.display = "none";
						}
						
						
					}

					function showStation(item, index) {
						routemap._stage.addChild(item['station_icon']);
						routemap._stage.addChild(item['station_text']);
					}
					
					function setupStation(item, index) {
						station = new Array();
						
						// icon
						station['station_icon'] = new createjs.Shape();
						graphics = station['station_icon'].graphics;
						graphics.setStrokeStyle(2);
						graphics.beginStroke("white");
						graphics.beginFill("#282460");
						graphics.drawCircle(item.pos_x, item.pos_y, 10);
						
						station['station_icon'].name = station['name'] = item.name;
						station['station_icon'].pos_x = station['pos_x'] = item.pos_x;
						station['station_icon'].pos_y = station['pos_y'] = item.pos_y;

						station['station_icon'].on("click", function () {
							showStationDetails(this);
						});
						
						// text
						station['station_text'] = new createjs.Text(item.name, '20px Arial', 'black');

						bounds = station['station_text'].getBounds();
						
						switch (item.text_position) {
							case 'left':
								station['station_text'].x = (parseInt(item.pos_x) - bounds.width - 20);
								station['station_text'].y = (parseInt(item.pos_y) - (bounds.height / 2));
								break;
								
							case 'right':
								station['station_text'].x = (parseInt(item.pos_x) + 20);
								station['station_text'].y = (parseInt(item.pos_y) - bounds.height / 2);
								break;
								
							case 'above':
								station['station_text'].x = (parseInt(item.pos_x) - 10);
								station['station_text'].y = (parseInt(item.pos_y) - (bounds.height / 2) - 20);
								break;
								
							case 'below':
								station['station_text'].x = (parseInt(item.pos_x) - 10);
								station['station_text'].y = (parseInt(item.pos_y) - (bounds.height / 2) + 20);
								break;
							
							default:
								//right
								station['station_text'].x = (parseInt(item.pos_x) + 20);
								station['station_text'].y = (parseInt(item.pos_y) - bounds.height / 2);
								break;
						}
						
						
						

						routemap.stations.push(station);
					}
					
					function showStationDetails(station) {
						var modal = document.getElementById('modal');
						var modal_content = document.getElementById('modal-content');

						var html = "";
						
						html += '<button id="modal-button" class="modal-button">X</button><br />';

						html += routemap._all_station_data[station.name].content;
						
						modal.style.display = "block";
						
						modal_content.innerHTML = html;
						
						var modal_button = document.getElementById('modal-button');

						modal_button.onclick = function() {
						    modal.style.display = "none";
						}
					}
					
					function handleTick() {
						routemap._stage.update();
					}
					
					function moveWorld(x_diff, y_diff) {
						
						for (var i = 0; i < routemap.stations.length; i++) {
							var current_station_icon = routemap.stations[i].station_icon;
							current_station_icon.x += x_diff;
							current_station_icon.y += y_diff;
							
							var current_station_text = routemap.stations[i].station_text;
							current_station_text.x = parseFloat(current_station_text.x) + x_diff;
							current_station_text.y = parseFloat(current_station_text.y) + y_diff;
							
						}
						
						for (var i = 0; i < routemap.routes.length; i++) {
							var current_route = routemap.routes[i];
							
							current_route.x = parseFloat(current_route.x) + x_diff;
							current_route.y = parseFloat(current_route.y) + y_diff;
						}
						
					}

				</script>
				
				<canvas id="myCanvas">
					Your Browser isn't Supported
				</canvas>

			</div>
			
			<div class="dt-12 align-central routelist">
				<div class="dt-12 align-central">
					<h3>Select Route</h3>
				</div>
				<?php
					$route_list_count = count($route_list);
					$number_of_columns = 3;
					$breakdown = ceil($route_list_count / $number_of_columns);
					
					$columns = array_chunk($route_list, $breakdown, true);

					$output_html = '';
					

					foreach ($columns as $column_key => $column_value) { 
						
						$output_html .= '<div class="dt-4 align-central">';
						foreach($column_value as $key => $value) {
							$output_html .= '<p onclick="' .  "showRoute('$key')" . '">' . $key .'</p>';
						}
		
						$output_html .= '</div>';
					}

					echo $output_html;
				?>

			</div>

		</div>
	</div>
</section>