<section class="comp comp-00501 <?php the_sub_field('background_colour'); ?>">
	<div class="wrapper no-gaps">
		<div class="row">
			<div class="dt-12 align-central">
				<h3><?php the_sub_field('title'); ?></h3>
			</div>
			<form action="" method="post">
				<div class="tp-10">
					<input class="<?php the_sub_field('background_colour'); ?>" type="text" name="email">
				</div>
				<div class="tp-2">
					<input class="primary" type="submit" value="Search">
				</div>
			</form>
		</div>
	</div>
</section>