<section class="comp comp-70101 <?php the_sub_field('background_colour'); ?> <?php the_sub_field('text_colour'); ?>">
	<div class="wrapper">
		<div class="row">
			<div class="dt-12 align-central">
				<h3>Sign up and Stay up to date</h3>
			</div>
			<?php if($_SERVER['REQUEST_METHOD'] == 'POST') { ?>
				<?php
					$headers = array('Content-Type: text/html; charset=UTF-8');
					$hcSubject = get_field('hc_subject', 'options');
					$email = $_POST['email'];
					
					$adminEmails = array();
					while(have_rows('receiver_email_addresses')) {
						the_row();
						array_push($adminEmails, get_sub_field('email_address'));
					}
					
					@wp_mail($adminEmails, 'Newsletter Sign Up', $email, $headers);
				?>
				<div class="dt-12 align-central">
					<p><?php the_sub_field('thank_you_message'); ?></p>
				</div>
			<?php } else { ?>
				<form action="" name="email-only-newsletter" method="post">
					<div class="tp-10">
						<input class="<?php the_sub_field('background_colour'); ?>" type="text" name="email" placeholder="Email Address">
					</div>
					<div class="tp-2">
						<input class="<?php the_sub_field('background_colour'); ?>" type="submit" value="Sign Up">
					</div>
				</form>
			<?php } ?>
		</div>
	</div>
</section>