<?php
	while(have_rows('layouts')) {
		the_row();
		if(get_row_layout() == '00101') {
			include('components/00101.php');
		} elseif(get_row_layout() == '00102') {
			include('components/00102.php');
		} elseif(get_row_layout() == '00201') {
			include('components/00201.php');
		} elseif(get_row_layout() == '00202') {
			include('components/00202.php');
		} elseif(get_row_layout() == '00301') {
			include('components/00301.php');
		} elseif(get_row_layout() == '00302') {
			include('components/00302.php');
		} elseif(get_row_layout() == '00303') {
			include('components/00303.php');
		} elseif(get_row_layout() == '00304') {
			include('components/00304.php');
		} elseif(get_row_layout() == '00401') {
			include('components/00401.php');
		} elseif(get_row_layout() == '00402') {
			include('components/00402.php');
		} elseif(get_row_layout() == '30201') {
			include('components/30201.php');
		} elseif(get_row_layout() == '30202') {
			include('components/30202.php');
		} elseif(get_row_layout() == '70101') {
			include('components/70101.php');
		} elseif(get_row_layout() == '70201') {
			include('components/70201.php');
		}
	}
?>