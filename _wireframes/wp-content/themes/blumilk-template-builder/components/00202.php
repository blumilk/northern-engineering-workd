<?php $bgColour = get_sub_field('background_colour'); ?>

<section class="comp comp-00202 <?php the_sub_field('background_colour'); ?>">
	<div class="wrapper <?php the_sub_field('width'); ?>">
		<div class="row">
			<?php while(have_rows('gallery')) {
				the_row(); ?>
				<div class="tl-3 tp-6 img">
					<?php $image = get_sub_field('image'); ?>
					<img src="<?php echo $image['url']; ?>">
					<?php if(get_sub_field('title') && get_sub_field('body_copy')) { ?>
						<div class="overlay <?php echo $bgColour; ?>">
							<p class="title">
								<?php the_sub_field('title'); ?>
							</p>
							<?php the_sub_field('body_copy'); ?>
							<a class="cta size-s primary" href="<?php the_sub_field('link'); ?>">
								Find Out More
							</a>
						</div>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
	</div>
</section>